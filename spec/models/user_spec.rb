require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_numericality_of(:age)
      .allow_nil
      .only_integer
      .is_greater_than_or_equal_to(0) }
    it { should validate_presence_of(:bio).allow_nil }
  end
end
