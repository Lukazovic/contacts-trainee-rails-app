class User < ApplicationRecord
  validates :name, presence: true
  validates :age, numericality: { allow_nil: true, greater_than_or_equal_to: 0,  only_integer: true }
end
